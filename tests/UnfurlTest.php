<?php

declare(strict_types=1);

namespace Breaker1\Unfurl;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class UnfurlTest extends TestCase
{
    public function testExpand()
    {
        $unfurl = new Unfurl();

        $this->assertEquals(
            [
                'This is only a test. I repeat, this is just a test',
                'This is only a test. I repeat, this  absolutely is not a test',
                'This is not a test. I repeat, this is just a test',
                'This is not a test. I repeat, this  absolutely is not a test',
            ],
            $unfurl->expand('This is {only,not} a test. I repeat, this {is just, absolutely is not} a test')
        );
    }

    public function testParsePattern()
    {
        $unfurl = new Unfurl();
        $this->assertEquals(
            [
                [
                    'man',
                    'woman',
                    'person',
                    'camera',
                    'tv'
                ],
                [
                    'today',
                    'now',
                    'tonight',
                    'this evening'
                ]
            ],
            $unfurl->parsePattern(
                'Hello {man,woman,person,camera,tv} how are you {today,now,tonight,this evening}?'
            )
        );
    }

    public static function parseExpressionProvider(): array
    {
        return [
            'zero to 5 by 1' => [
                '0..5',
                [0,1,2,3,4,5]
            ],
            '5 to 0 by 1' => [
                '5..0',
                [5,4,3,2,1,0]
            ],
            '0 to 10 by 5' => [
                '0..10..5',
                [0,5,10]
            ],
            'a to f by 1' => [
                'a..f',
                ['a', 'b', 'c', 'd', 'e', 'f']
            ],
            'a..f by 2' => [
                'a..f..2',
                ['a', 'c', 'e']
            ],
            'fruit' => [
                'apples,oranges,bananas,grapes',
                [
                    'apples',
                    'oranges',
                    'bananas',
                    'grapes',
                ]
            ],
            'null first value fruit' => [
                ',apples,oranges,bananas,grapes',
                [
                    '',
                    'apples',
                    'oranges',
                    'bananas',
                    'grapes',
                ]
            ]
        ];
    }

    /**
     * @param string $expression
     * @param array $expected
     */
    #[DataProvider('parseExpressionProvider')]
    public function testParseExpression(string $expression, array $expected)
    {
        $unfurl = new Unfurl();
        $this->assertEquals(
            $expected,
            iterator_to_array($unfurl->parseExpression($expression))
        );
    }

    public static function makeIntRangeProvider(): array
    {
        return [
            'Zero to three' => [
                0,
                3,
                1,
                [0, 1, 2, 3],
            ],
            'Negative one to 3' => [
                -1,
                3,
                1,
                [-1, 0, 1, 2, 3],
            ],
            'Three to zero' => [
                3,
                0,
                1,
                [3, 2, 1, 0],
            ],
            'Zero pad 0 to 5' => [
                0,
                5,
                1,
                [00, 01, 02, 03, 04, 05],
            ],
        ];
    }

    /**
     * @dataProvider makeIntRangeProvider
     * @param int $start
     * @param int $stop
     * @param int $step
     * @param array $expected
     */
    #[DataProvider('makeIntRangeProvider')]
    public function testMakeIntRange(int $start, int $stop, int $step, array $expected)
    {
        $unfurl = new Unfurl();

        $this->assertEquals(
            $expected,
            iterator_to_array($unfurl->makeIntRange($start, $stop, $step))
        );
    }

    public static function makeCharRangeProvider(): array
    {
        return [
            'a to c' => [
                'a',
                'c',
                1,
                ['a', 'b', 'c'],
            ],
            'c to a' => [
                'c',
                'a',
                1,
                ['c', 'b', 'a']
            ]
        ];
    }

    /**
     * @param string $start
     * @param string $stop
     * @param int $step
     * @param array $expected
     */
    #[DataProvider('makeCharRangeProvider')]
    public function testMakeCharRange(string $start, string $stop, int $step, array $expected)
    {
        $unfurl = new Unfurl();

        $this->assertEquals(
            $expected,
            iterator_to_array($unfurl->makeCharRange($start, $stop, $step))
        );
    }
}
