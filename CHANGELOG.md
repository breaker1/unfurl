# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/).

## [1.0.0] - 2021-05-06
* New: initial release of 'breaker1/unfurl'
