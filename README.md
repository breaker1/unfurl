# Brace Expansion
Bash-style brace expansion for PHP.

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-travis]][link-travis]
[![Coverage Status][ico-scrutinizer]][link-scrutinizer]
[![Quality Score][ico-code-quality]][link-code-quality]
[![Total Downloads][ico-downloads]][link-downloads]

## Structure

If any of the following are applicable to your project, then the directory structure should follow industry best practices by being named the following.

```
build/
src/
tests/
vendor/
```


## Install

Via Composer

``` bash
$ composer require breaker1/unfurl
```

## Usage

``` php
$unfurl = new Breaker1\Unfurl\Unfurl();
print_r($unfurl->expand('I do {,not} like examples all that much.'));
```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md) for details.

## Security

If you discover any security related issues, please email breaker1@protonmail.com instead of using the issue tracker.

## Credits

- [Anthony Vitacco][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/breaker1/unfurl.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/breaker1/unfurl/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/breaker1/unfurl.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/breaker1/unfurl.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/breaker1/unfurl.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/breaker1/unfurl
[link-travis]: https://travis-ci.com/gitlab/breaker1/unfurl
[link-scrutinizer]: https://scrutinizer-ci.com/g/breaker1/unfurl/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/breaker1/unfurl
[link-downloads]: https://packagist.org/packages/breaker1/unfurl
[link-author]: https://github.com/breaker1
[link-contributors]: ../../contributors
