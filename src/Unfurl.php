<?php

declare(strict_types=1);

namespace Breaker1\Unfurl;

use function BenTools\CartesianProduct\cartesian_product;

/**
 * Class Unfurl
 */
class Unfurl
{
    protected string $intRange = '/^(-?\d+)\.\.(-?\d+)(?:\.\.-?(\d+))?$/';
    protected string $charRange = '/^([A-Za-z])\.\.([A-Za-z])(?:\.\.-?(\d+))?$/';

    /**
     * Take a given string and expand the braces
     * @param string $input
     * @return array<int, string>
     */
    public function expand(string $input): array
    {
        $subs = $this->parsePattern($input);
        $substitutablePattern = preg_replace(
            '/{.*?}/',
            '%s',
            $input
        );
        $expansions = [];
        foreach (cartesian_product($subs) as $combination) {
            $expansions[] = vsprintf($substitutablePattern, $combination);
        }
        return $expansions;
    }

    /**
     * @param string $pattern
     * @return array<int, array>
     */
    public function parsePattern(string $pattern): array
    {
        $start = 0;
        $position = 0;
        $bracketDepth = 0;
        $items = [];

        while ($position < strlen($pattern)) {
            if ($pattern[$position] == '{') {
                $start = $position;
                $bracketDepth++;
            }
            if ($pattern[$position] == '}') {
                $bracketDepth--;
                $expression = substr($pattern, $start + 1, $position - $start - 1);
                $items[] = iterator_to_array($this->parseExpression($expression));
            }
            $position++;
        }

        return $items;
    }

    /**
     * @param string $expression
     * @return iterable
     */
    public function parseExpression(string $expression): iterable
    {
        $intRangeMatch = preg_match($this->intRange, $expression, $intRangeMatches);
        if ($intRangeMatch) {
            $step = 1;
            if (isset($intRangeMatches[3])) {
                $step = $intRangeMatches[3];
            }
            return $this->makeIntRange(
                (int)$intRangeMatches[1],
                (int)$intRangeMatches[2],
                (int)$step
            );
        }

        $charRangeMatch = preg_match($this->charRange, $expression, $charRangeMatches);
        if ($charRangeMatch) {
            $step = 1;
            if (isset($charRangeMatches[3])) {
                $step = $charRangeMatches[3];
            }
            return $this->makeCharRange(
                $charRangeMatches[1],
                $charRangeMatches[2],
                (int)$step
            );
        }

        return $this->makeValueSet($expression);
    }

    /**
     * @param int $start
     * @param int $stop
     * @param int $step
     * @return iterable<int>
     */
    public function makeIntRange(int $start, int $stop, int $step = 1): iterable
    {
        foreach (range($start, $stop, $step) as $val) {
            yield($val);
        }
    }

    /**
     * This function exists to create ranges of letters
     * @param string $start
     * @param string $stop
     * @param int $step
     * @return iterable<string>
     */
    public function makeCharRange(string $start, string $stop, int $step = 1): iterable
    {
        foreach (range($start, $stop, $step) as $val) {
            yield($val);
        }
    }

    /**
     * This function creates a value set iterator
     * @param string $input
     * @return iterable
     */
    public function makeValueSet(string $input): iterable
    {
        foreach (explode(',', $input) as $val) {
            yield($val);
        }
    }
}
